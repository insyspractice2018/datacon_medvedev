import { Component } from '@angular/core';

@Component({
    selector: 'search-comp',
    template: `     <header class="search-container">
                        <mat-icon class="search-icon">search</mat-icon>
                        <mat-form-field class="search-input">
                            <input type="serch" matInput placeholder="Поиск по проектам ..."/>
                        </mat-form-field>
                        <div class="login">
                            <label>Тут должно быть чьё-то имя и фамилия</label>
                            <mat-icon>power_settings_new</mat-icon>
                            <span>   Выход</span>
                        </div>
                    </header>`,
    styleUrls: [ './search.component.css' ]
})

export class SearchComponent { }