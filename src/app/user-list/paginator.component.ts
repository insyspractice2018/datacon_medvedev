import {Component} from '@angular/core';

@Component({
  selector: 'paginator-comp',
  template: `<mat-paginator [length]="100"
                [pageSize]="10"
                [pageSizeOptions]="[5, 10, 25, 100]">
            </mat-paginator>`,
  styles: [`
            mat-paginator {
              width: 88%;
            }
          `]
})
export class PaginatorComponent {}