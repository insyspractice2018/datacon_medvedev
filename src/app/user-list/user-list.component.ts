import { Component, OnInit, ViewChild } from '@angular/core';
import { UserListService } from './user-list.service';
import { Project } from './project';
import { MatSort, MatTableDataSource } from '@angular/material';
import { PaginatorComponent } from './paginator.component';
import { SearchComponent } from '../shared/search.component';

@Component({
    selector: 'user-list',
    templateUrl: 'user-list.component.html',
    styleUrls: [ 'user-list.component.css' ],
    providers: [ UserListService ]
})

export class UserListComponent implements OnInit {

    projects: Project[] = [];

    constructor(private userListService: UserListService) {
    }

    displayedColumns: string[] = ['name', 'place', 'grade', 'client', 'status', 'change'];
    dataSource = new MatTableDataSource(this.projects);

    @ViewChild(MatSort) sort: MatSort;

    ngOnInit() {

        this.dataSource.sort = this.sort;
        this.userListService.getProjects().subscribe(data => {
        this.projects = data;
        this.dataSource = new MatTableDataSource(this.projects);
            console.log(this.projects)
        });
    }
} 