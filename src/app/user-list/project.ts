export class Project {

    name: string;
    place: string;
    grade: string;
    client: string;
    status: string;
    change: any;
}