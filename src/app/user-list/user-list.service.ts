import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from './project';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
   
@Injectable()
export class UserListService{
   
    constructor(private http: HttpClient){ }
       
    getProjects() : Observable<Project[]> {
        return this.http.get('projects.json').pipe(map(data=>{
            let projectList = data["projectList"];
            return projectList.map(function(project:any) {
                return {name: project.name, 
                        place: project.place, 
                        grade: project.grade, 
                        client: project.client, 
                        status: project.status};
              });
        }));
    }
}