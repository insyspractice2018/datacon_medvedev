import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';
import { MaterialModule } from './material.module';
import { NavigatorComponent } from './shared/navigator.component';
import { UserListComponent } from './user-list/user-list.component';
import { PaginatorComponent } from './user-list/paginator.component';
import { SearchComponent } from './shared/search.component';
import { AppComponent } from './app.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { EmailComponent } from './shared/email.component';

@NgModule({
    imports:    [ 
                    BrowserModule, 
                    FormsModule, 
                    HttpClientModule, 
                    MaterialModule
                ],
    declarations: [ AppComponent, 
                    // UserListComponent, 
                    UserDetailsComponent,
                    // PaginatorComponent,
                    SearchComponent,
                    NavigatorComponent,
                    EmailComponent                
                ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
