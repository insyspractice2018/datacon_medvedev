import { NgModule } from '@angular/core';
import { MatSortModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
    imports:    [ 
                    MatSortModule,  
                    MatTableModule,
                    BrowserAnimationsModule,
                    MatPaginatorModule,
                    MatButtonModule,
                    MatIconModule,
                    MatMenuModule,
                    MatListModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatSelectModule
                ],
    exports:    [ 
                    MatSortModule,  
                    MatTableModule,
                    BrowserAnimationsModule,
                    MatPaginatorModule,
                    MatButtonModule,
                    MatIconModule,
                    MatMenuModule,
                    MatListModule,
                    MatInputModule,
                    MatFormFieldModule,
                    MatSelectModule
                ]
})
export class MaterialModule { }